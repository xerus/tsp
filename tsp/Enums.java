package tsp;

/**
 * Metoda křížení
 */
enum CrossoverType {
	PMX, OX, ERX, SCX, MSCX, ONE_POINT, TWO_POINT, MULTI_POINT, NO_CROSSOVER;

   public CrossoverType getNext() {
	     return this.ordinal() < CrossoverType.values().length - 1
	         ? CrossoverType.values()[this.ordinal() + 1]
	         : null;
	   }


	public String toString() {
		String s = new String();
		switch (this) {
			case PMX:
				s = "Partially Matched Crossover";
				break;
			case OX:
				s = "Order Crossover";
				break;
			case ERX:
				s = "Edge Recombination Crossover";
				break;
			case SCX:
				s = "Sequential Constructive Crossover";
				break;
			case MSCX:
				s = "modified SCX";
				break;
			case ONE_POINT:
				s = "Jednobodové indexové křížení";
				break;
			case TWO_POINT:
				s = "Dvoubodové indexové křížení";
				break;
			case MULTI_POINT:
				s = "Vícebodové indexové křížení";
				break;
			case NO_CROSSOVER:
				s = "Žádné křížení";
				break;
			default:
				s = "";
				break;
		}
		return s;
	}

}

/**
 * Metody mutace
 */
enum MutationType {
	INVERSION, EXCHANGE, SHUFFLE;

	public String toString() {
		String s = new String();
		switch(this) {
			case EXCHANGE:
				s = "Vzájemná výměna dvou měst";
				break;
			case INVERSION:
				s = "Inverze úseku cesty";
				break;
			case SHUFFLE:
			default:				
				s = "Zamíchá pořadí měst";
				break;
		}
		return s;
	}
}

/**
 * Způsob výběru rodičů
 */
enum ParentChooseType {
	RULETE, SUS;

	public String toString() {
		String s = new String();
		switch (this) {
			case RULETE:
				s = "Ruletové kolo";
				break;
			case SUS:
			default:
				s = "Stochastické univerzální vzorkování";
				break;
		}
		return s;
	}
}

/**
 * Způsob zařazení do populace
 */
enum ReinsertionType {
	FITNESS_BASED, PURE, UNIFORM, QUARTER;

	public String toString() {
		String s = new String();
		switch (this) {
			case PURE:
				s = "Pouze potomci";
				break;
			case FITNESS_BASED:
				s = "Pouze nejlepší jedinci";
				break;
			case QUARTER:
				s = "Čtvrtina rodičů";
				break;
			case UNIFORM:
			default:
				s = "Rovnoměrně rodiče i děti";
				break;
		}
		return s;
	}
}

/** 
 * Představuje stav aplikace
 * Buď může být prováděn pouze jeden krok, nebo řada kroků, nebo může být pozastavená
 */
enum ApplicationState {
	STEP, RUNNING, STOP;
}