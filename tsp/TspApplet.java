package tsp;

import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Vector;

/**
 * Třída používaná pro spuštění jako applet
 */
public class TspApplet extends JApplet {
	Control t;
	public void init() {
		t  = new Control();
		try {
			showStatus("Applet se načítá");
			javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
				public void run() {
					new Main(getContentPane() ,getCodeBase(), t);
					showStatus("Applet načten");
				}
			});
		} catch (Exception e) {
			showStatus("Applet se nepodařilo spustit");
		}
	}

	public void stop() {
		if (t != null) {
			t.setStopped();
			t.join();
			t = null; 
		}
	}
}
