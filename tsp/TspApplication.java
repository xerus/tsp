package tsp;

import java.awt.*;
import javax.swing.*;


/**
 * Třída používaná pro spuštění aplikace
 */
public class TspApplication {
	public static void main(String[] args) {
		try {
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					Main window = new Main();					
					window.setVisible(true);
				}
			});
		} catch (Exception e) {
			System.err.println("createGUI didn't successfully complete");
			System.exit(1);
		}
	}
}