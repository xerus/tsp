package tsp;

class Control implements Runnable {
	boolean suspended;
	boolean stopped;
	Thread t;

	int lastlen;
	int bestgen;

	Control() {
		t = new Thread(this);
		suspended = false;
		stopped = false;
		lastlen = 0;
		bestgen = 0;
	}

	public void run() {
		try {
			while (true) {
				synchronized (this) {
					while (suspended) {
						wait();
					}
					if (stopped) break;
				}
				switch (Main.STATE) {
					case STEP:
						Main.STATE = ApplicationState.STOP;
					case RUNNING:
						step();
						break;
					case STOP:
					default:
						setSuspended();
						break;
				}
			}
		} catch (InterruptedException e) {
			System.out.println("Vlákno bylo přerušeno");
		}
	}

	synchronized void setSuspended() {
		suspended = true;
	}

	synchronized boolean getState() {
		return suspended;
	}

	synchronized void setUnsuspended() {
		suspended = false;
		notify();
	}

	synchronized void setStopped() {
		stopped = true;
		suspended = false;
		notify();
	}

	public void start() {		
		t.start();
	}

	public void join() {
		try {
			t.join();
		}
		catch (Exception e) {
			System.err.println("Vlákno Control neskončilo: "+e);
		}
	}

	private void step() {
		if (bestgen >= Main.generationNum) {
			bestgen = 0;
			lastlen = 0;
		}

		int i = Main.p.getBestChromosomeIndex(); // index nejlepšího jedince v populaci
		Chromosome ch = Main.p.getChromosome(i); // nejlepší jedinec v populaci
		int len = Main.p.getChromosomeRouteLength(i); // délka cesty tohoto jedince
		boolean change = (lastlen != len); // indikuje, zda se změnila délka
		if (change) {
			bestgen = Main.generationNum;
		}
		Main.drawArea.setRoute(ch.getConfiguration(), len, (change) ? Main.generationNum : bestgen); // nastavení cesty
		Population children = Main.p.nextGeneration(Main.CROSSOVER, Main.CROSSOVER_COEFICIENT, Main.MUTATION, Main.MUTATION_COEFICIENT, Main.PARENT_CHOOSE);
		Main.p = Main.p.mixPopulations(children, Main.REINSERTION);
		Main.drawArea.setTitle("Nejlepší jedinec generace "+Main.generationNum);
		Main.generationNum++;

		lastlen = len;
	}
}