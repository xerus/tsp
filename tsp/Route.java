package tsp;

import java.awt.*;
import javax.swing.*;
import java.awt.geom.*;
import java.util.Arrays;

class Route extends JPanel {
	Point2D.Double[] points; // souřadnice bodů
	int[] route;	// cestu představuje seznam uzlů - konfigurace chromozomu 

	int RECT_WIDTH = 500;
	int RECT_HEIGHT = 500;
	int POINT_SIZE = 4;
	int BOUND = 5*POINT_SIZE;


	public Route() {
		points = null;
		route = null;
		Dimension d = new Dimension(RECT_WIDTH+2*BOUND,RECT_HEIGHT+2*BOUND);
		setSize(d);
		setPreferredSize(d);
	}

	/**
	 * Nastavení názvů a souřadnic měst
	 * Podle nich se poté vykreslují body
	 * @param coordinates Souřadnice
	 */
	public void setCities(double[][] coordinates) {
		this.points = normalizeCoordinates(coordinates);
		this.route = null;
		this.repaint(); // překreslení
	}

	/**
	 * Nastaví cestu mezi městy, ta se vykreslí
	 * @param route Pořadí měst v cestě
	 */
	public void setRoute(int[] route) {
		if  (Arrays.equals(this.route, route) != true) { // vykresluje se pouze pokud dojde ke zmene cesty
			this.route = route;
			this.repaint();			
		}
	}


	/**
	 * Převede souřadnice na body, zároveň je upraví tak, aby seděli
	 * do vykreslovaného obdélníku
	 * @param coordinates Souřadnice bodů
	 */
	private Point2D.Double[] normalizeCoordinates(double[][] coordinates) {
		double scale, tmp;
		double maxx, maxy, minx, miny;
		Point2D.Double[] p = new Point2D.Double[coordinates.length];
		minx = coordinates[0][0]; maxx = coordinates[0][0];
		miny = coordinates[0][1]; maxy = coordinates[0][1];

		for (int i = 1; i < coordinates.length; i++) { // najdění minima a maxima
			if (coordinates[i][0] > maxx) maxx = coordinates[i][0];
			if (coordinates[i][1] > maxy) maxy = coordinates[i][1];
			if (coordinates[i][0] < minx) minx = coordinates[i][0];
			if (coordinates[i][1] < miny) miny = coordinates[i][1];
		}

		maxx -= minx;
		maxy -= miny;

		// vypočítání poměru
		scale = (maxx != 0) ? (double) (RECT_WIDTH - 2 * BOUND) / maxx : 1;
		tmp = (maxy != 0) ? (double) (RECT_HEIGHT - 2 * BOUND) / maxy : 1;
		if (tmp < scale) scale = tmp;

		// vlastní normalizace
		for (int i = 0; i < coordinates.length; i++) {
			double x = (coordinates[i][0] - minx) ;
			double y = (coordinates[i][1] - miny) ;
				x *= scale;
				y *= scale;
					
		p[i] = new Point2D.Double(y + BOUND, RECT_HEIGHT - (x + BOUND));
		}

		return p;
	}

	/**
	 * Provádí vykreslení komponenty
     * Vykreslí obdélník, následně do něj nakreslí body představující města
     * Má-li informaci o trase, tak i ji vykreslí
	 */
	@Override
	public void paintComponent(Graphics g) {
			super.paintComponent(g);

			Graphics2D g2d = (Graphics2D) g;

			g2d.setColor(Color.white);
			g2d.fill(new Rectangle(0, 0, RECT_WIDTH, RECT_HEIGHT)); // nakreslení pozadí
			g2d.setColor(Color.black);
			g2d.draw(new Rectangle(0, 0, RECT_WIDTH, RECT_HEIGHT)); // nakreslení obrysu

			if (route != null) { // vykreslení cesty
				// čáry mezi body
				for (int i = 0; i < route.length - 1; i++) {
					g2d.draw(new Line2D.Double(points[route[i]], points[route[i+1]]));
				}
				g2d.draw(new Line2D.Double(points[route[route.length - 1]], points[route[0]]));
			}

			g2d.setColor(Color.red);

			if (points != null) { // vykreslení bodů
				for (Point2D.Double p : points) {
					double x, y;
					x = p.getX() - POINT_SIZE / 2;
					y = p.getY() - POINT_SIZE / 2;

					g2d.fill(new Ellipse2D.Double(x, y, POINT_SIZE, POINT_SIZE));
				}
			}

	}
}