package tsp;

public class Condition {
	private ConditionType type;
	private double value;
	private double l_bound;
	private double h_bound;

	public Condition() {
		this.type = ConditionType.TRUE;
		this.value = 0;
		this.l_bound = 0;
		this.h_bound = 0;
	}


	public Condition(ConditionType type, double value) {
		this.setTypeAndValue(type, value);
	}

	public Condition(Condition c) {
		this.copyCondition(c);
	}

	public Condition(double l_bound, double h_bound) {
		this.setRange(l_bound, h_bound);
	}

	public Condition(Condition first, Condition second) {
		ConditionType ftype = first.getType();
		ConditionType stype = second.getType();
		if (ConditionType.TRUE == ftype) {
			copyCondition(second);
		} else if (ConditionType.TRUE == stype) {
			copyCondition(first);
		} else if (ftype == stype) {
			if (ConditionType.LESS == ftype) {
				setTypeAndValue(ftype, Math.min(first.getValue(), second.getValue()));
			} else if (ConditionType.MORE == ftype) {
				setTypeAndValue(ftype, Math.max(first.getValue(), second.getValue()));
			} else { // RANGE
				setRange(Math.max(first.getLowBound(), second.getLowBound()),
					Math.min(first.getHighBound(), second.getHighBound()));
			}
		} else { // ftype != stype
			if (ConditionType.RANGE == ftype) {
				if (ConditionType.LESS == stype) {
					setRange(first.getLowBound(), Math.min(first.getHighBound(), second.getValue()));
				} else { // MORE
					setRange(Math.max(first.getLowBound(), second.getValue()), first.getHighBound());
				}
			} else if (ConditionType.RANGE == stype) {
				if (ConditionType.LESS == ftype) {
					setRange(second.getLowBound(), Math.min(second.getHighBound(), first.getValue()));
				} else { // MORE
					setRange(Math.max(second.getLowBound(), first.getValue()), second.getHighBound());
				}
			} else { // MORE & LESS
				if (ConditionType.LESS == ftype) {
					setRange(second.getValue(), first.getValue());
				} else {
					setRange(first.getValue(), second.getValue());
				}
			}
		}
	}


	private void copyCondition(Condition c) {
		this.type = c.getType();
		this.value = c.getValue();
		this.l_bound = c.getLowBound();
		this.h_bound = c.getHighBound();
	}

	private void setTypeAndValue(ConditionType type, double value) {		
		if (ConditionType.RANGE != type) {
			this.type = type;
			this.value = value;
			this.l_bound = 0;
			this.h_bound = 0;
		}
	}

	private void setRange(double l_bound, double h_bound) {
		if (l_bound > h_bound) {
			this.type = ConditionType.TRUE;
			this.value = 0;
			this.l_bound = 0;
			this.h_bound = 0;
		} else {
			this.type = ConditionType.RANGE;
			this.value = 0;
			this.l_bound = l_bound;
			this.h_bound = h_bound;
		}
	}

	public boolean cover(double value) {
		switch(this.getType()) {
		case TRUE:
			return true;
		case RANGE:
			return value > this.getLowBound() && value < this.getHighBound();
		case LESS:
			return value < this.getValue();
		case MORE:
			return value > this.getValue();
		}
		return false;
	}

	ConditionType getType() {
		return this.type;
	}

	double getValue() {
		return this.value;
	}

	double getLowBound() {
		return this.l_bound;
	}

	double getHighBound() {
		return this.h_bound;
	}

	public String toString() {
		switch(this.getType()) {
			case RANGE:
				return "RANGE: [" + this.getLowBound() + ", " + this.getHighBound() + "]";
			case TRUE:
				return "TRUE";
			default:
				return this.getType() + ": " + this.getValue();
		}
	}
}