package tsp;

import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Vector;


/**
 * Třída zajišťuje vytvoření grafického rozhraní a spouštění výpočtu
 */
public class Main extends JFrame {
	DataFile[] data;
	Control c;
	// formulářové položky
	JSpinner population;
	JComboBox method;
	JSpinner crossoverCoef;
	JComboBox city;
	JComboBox mutation;
	JSpinner mutationCoef;
	JComboBox parentChoose;
	JComboBox reinsertion;
	// popisek
	JLabel description;

	JButton start, krok, reset;
	private JTextArea textArea;
	
	public static volatile Population p;
	public static volatile DrawArea drawArea;
	public static volatile int generationNum = 0;
	public static volatile CrossoverType CROSSOVER;
	public static volatile float CROSSOVER_COEFICIENT = 0.9f;
	public static volatile ParentChooseType PARENT_CHOOSE;
	public static volatile ReinsertionType REINSERTION;
	public static volatile int POPULATION_SIZE = 25;
	public static volatile MutationType MUTATION;
	public static volatile float MUTATION_COEFICIENT = 0.01f;
	public static volatile ApplicationState STATE = ApplicationState.STOP;


	String TSP_LIST = "tspfiles.txt"; // soubor obsahuje cesty k souborům s městy
	String DELIMITER = ":"; // oddělovač v TSP_LIST

	private boolean isApplet;
	private Container container;
	private URL codebase;

	Main() {
		isApplet = false;
		loadData(); // načtení a zpracování dat
		setUpGUI(); // nastavení GUI
		c = new Control();
		c.start();
		reset();
	}

	Main(Container container, URL codebase, Control t) {
		isApplet = true;
		this.codebase = codebase;
		this.container = container;
		loadData(); // načtení a zpracování dat
		setUpGUI(); // nastavení GUI
		c = t;
		c.start();
		reset();
	}

	/**
	 * Vytvoří lištu s ovládáním, obsluhy tlačítek
	 */
	public void setUpGUI() {
		// hlavní kontejner
		Container content;
		if (isApplet) {
			content = container;
		} else {
			content = new JPanel();
		}
		content.setLayout(new BorderLayout(10, 10));

		SpinnerNumberModel sm = new SpinnerNumberModel();
		sm.setValue(POPULATION_SIZE); // velikost populace
		sm.setMinimum(2);
		sm.setStepSize(1);
		// nastavovací okno
		population = new JSpinner(sm);
		city = new JComboBox(data);
			city.setBackground(Color.white);
		method = new JComboBox(CrossoverType.values());
			method.setBackground(Color.white);
		crossoverCoef = new JSpinner(new SpinnerNumberModel(CROSSOVER_COEFICIENT * 100, 0, 100, 1));
		mutation = new JComboBox(MutationType.values());
			mutation.setBackground(Color.white);
		mutationCoef = new JSpinner(new SpinnerNumberModel(MUTATION_COEFICIENT * 1000, 0, 1000, 1));
		parentChoose = new JComboBox(ParentChooseType.values());
			parentChoose.setBackground(Color.white);
		reinsertion = new JComboBox(ReinsertionType.values());
			reinsertion.setBackground(Color.white);

		final JComponent[] popup = new JComponent[] {
			new JLabel("Sada měst: "),
			city,
			new JLabel("Velikost populace: "),
			population,
			new JLabel("Metoda křížení: "),
			method,
			new JLabel("Pravděpodobnost křížení [%]: "),
			crossoverCoef,
			new JLabel("Metoda mutace: "),
			mutation,			
			new JLabel("Pravděpodobnost mutace [\u2030]: "),
			mutationCoef,
			new JLabel("Výběr rodičů ke křížení:"),
			parentChoose,
			new JLabel("Složení nové populace:"),
			reinsertion,
		};
		// okno s výpisem cesty
		JButton show = new JButton("Zobraz cestu");

		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		JScrollPane scrollArea = new JScrollPane(textArea);
		scrollArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollArea.setPreferredSize(new Dimension(200, 200));
		
		final JComponent[] showF = {
			new JLabel("Cesta mezi městy"),
			scrollArea
		};


		// North - lišta s akcemi
		JPanel north = new JPanel();
		north.setLayout(new GridLayout(1, 5, 15, 5));

		description = new JLabel();
		// Start, Krok, Reset
		start = new JButton("start");
		krok = new JButton("krok");
		reset = new JButton("reset");
		// pokročilé nastavení
		JButton advanced = new JButton("nastavení");

		// vložení do kontejneru north
		north.add(start);
		north.add(krok);
		north.add(reset);
		north.add(description);
		north.add(advanced);
		
		JPanel west = new JPanel();
		west.setPreferredSize(new Dimension(300,500));
		west.add(description);

		content.add(north, BorderLayout.NORTH);

		content.add(west, BorderLayout.WEST);
		
		// Center - nejlepší jedinec
		drawArea = new DrawArea();
		content.add(drawArea, BorderLayout.CENTER);

		content.add(show, BorderLayout.SOUTH);

		//... Set window characteristics.
		if (isApplet == false) {
			setContentPane(content);
			setTitle("TSP pomocí genetických algoritmů");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			pack();
		}

		// nastavení akcí při stisku tlačítek
		// advanced
		advanced.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ua) {
				if (STATE == ApplicationState.RUNNING) start.setText("pokračovat");
				STATE = ApplicationState.STOP;
				while (c.getState() == false);
				DataFile d = getDataFile();
				int answer = JOptionPane.showConfirmDialog(null, popup, "Nastavte vlastnosti genetického algoritmu", JOptionPane. OK_CANCEL_OPTION);
				if (answer == 0) { // úspěšně proběhl
					if ((d != getDataFile()) || (POPULATION_SIZE != getPopulationSize())) {
						reset();
					}
					else {
						setActualParameters();
						setDescription();
					}
				}
				c.setUnsuspended();
			}
		});
		// start
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ua) {
				startCallback();
			}
		});
		// krok
		krok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ua) {
				if (p == null) reset();
				if (STATE == ApplicationState.RUNNING) start.setText("pokračovat");
				STATE = ApplicationState.STEP;
				c.setUnsuspended();
			}
		});
		// reset
		reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ua) {
				start.setText("Start");
				reset();
			}
		});
		// show
		show.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ua) {
				if (STATE == ApplicationState.RUNNING) start.setText("pokračovat");
				STATE = ApplicationState.STOP;
				while (c.getState() == false);
				textArea.setText(drawArea.getNormalizedRouteText());
				JOptionPane.showMessageDialog(null, showF, "Cesta mezi městy", JOptionPane.PLAIN_MESSAGE);				
			}
		});
	}
		
	public void startCallback() {
		if (p == null) reset();
		switch (STATE) {
			case RUNNING: // kliknutí na "pozastavit"
				STATE = ApplicationState.STOP;
				while (c.getState() == false);
				start.setText("pokračovat");
				break;			
			default: // kliknutí na "start"	
			case STOP:
			case STEP:
				STATE = ApplicationState.RUNNING;
				c.setUnsuspended();
				start.setText("pozastavit");
				break;
		}
	}

	/**
	 * Slouží pro počáteční inicializaci populace
	 * Provádí se při změně velikosti populace, změně sady měst nebo po stisku tlačítka reset
	 */
	public void reset() {
		STATE = ApplicationState.STOP;
		while (c.getState() == false);
		// získání aktuálních hodnot
		setActualParameters();

		generationNum = 0;
		start.setText("start");
		textArea.setText("");
		drawArea.clear(); // vymazání všeho		
	
		setDescription();
		DataFile d = getDataFile();
		drawArea.setCities(d.getCityNames());
		drawArea.getRouteElement().setCities(d.getCoordinates()); // nakreslení souřadnic měst

			p = new Population(POPULATION_SIZE, d.getDistanceMatrix());
	}

	private void setDescription() {
		String s = new String("<html><h2><u>Aktuální nastavení:</u></h2><br>");
		s += "<h3><u>Sada měst:</u></h3> "  + getDataFile() + "<br>";
		s += "<h3><u>Velikost populace:</u></h3> " + POPULATION_SIZE + "<br>";
		s += "<h3><u>Typ křížení:</u></h3> " + CROSSOVER + "<br>";
		s += "<h3><u>Pravděpodobnost křížení:</u></h3> " + CROSSOVER_COEFICIENT * 100 + " % <br>";
		s += "<h3><u>Typ mutace:</u></h3> " + MUTATION + "<br>";
		s += "<h3><u>Pravděpodobnost mutace:</u></h3> " + MUTATION_COEFICIENT * 100 + " % <br>";
		s += "<h3><u>Výběr rodičů:</u></h3> " + PARENT_CHOOSE + "<br>";
		s += "<h3><u>Složení nové populace:</u></h3> " + REINSERTION + "<br>";
		s += "</html>";
		description.setText(s);
	}

	private void setActualParameters() {
		CROSSOVER = getCrossoverType();
		CROSSOVER_COEFICIENT = getCrossoverCoeficient();
		PARENT_CHOOSE = getParentChooseType();
		REINSERTION = getReinsertionType();
		POPULATION_SIZE = getPopulationSize();
		MUTATION = getMutationType();
		MUTATION_COEFICIENT = getMutationCoeficient();		
	}

	/**
	 * Slouží pro zjištění zvolené metody křížení
	 * @return zvolené metoda
	 */
	private CrossoverType getCrossoverType() {
		return (CrossoverType) method.getSelectedItem();
	}

	/**
	 * Slouží pro zjištění zvolené metody křížení
	 * @return zvolené metoda
	 */
	private MutationType getMutationType() {
		return (MutationType) mutation.getSelectedItem();
	}

	private ParentChooseType getParentChooseType() {
		return (ParentChooseType) parentChoose.getSelectedItem();
	}

	private ReinsertionType getReinsertionType() {
		return (ReinsertionType) reinsertion.getSelectedItem();
	}
	
	/**
	 *
	 */
	private float getMutationCoeficient() {
		return Float.parseFloat(mutationCoef.getValue().toString()) / 1000;		
	}
	
	/**
	 *
	 */
	private float getCrossoverCoeficient() {
		return Float.parseFloat(crossoverCoef.getValue().toString()) / 100;		
	}

	/**
	 *
	 */
	private DataFile getDataFile() {
		return (DataFile) city.getSelectedItem();
	}

	/**
	 *
	 */
	private int getPopulationSize() {
		return Integer.parseInt(population.getValue().toString());
	}

	private URL createUrl(String filename) throws MalformedURLException {
		URL u;
		if (isApplet) {
			u = new URL(codebase, filename);
		} else {
			u = new URL("file", "", filename);
		}
		return u;
	}

	public void loadData() {
		BufferedReader br;
		String line = new String();
		Vector<String> v = new Vector<String>();
		// otevření TSP_LIST a načtení
		try {
			URL source = createUrl(TSP_LIST);
			br = new BufferedReader(new InputStreamReader(source.openStream()));
			// načtení celého TSP_LIST
			while((line = br.readLine()) != null) v.addElement(line);
		} catch (Exception e) {
			System.err.println("Nepodařilo se otevřít soubor "+TSP_LIST);
			System.exit(1);
		}
		data = new DataFile[v.size()];
		// zpracování jednotlivých řádků z TSP_LIST
		for (int i = 0; i < data.length; i++) {
			String[] parsed;
			line = v.get(i);
			parsed = line.split(DELIMITER);
			try {
				URL coordFile = createUrl(parsed[0]);
				switch (parsed.length) {
				case 1: 
					data[i] = new DataFile(coordFile);
					break;
				case 2: 
					data[i] = new DataFile(coordFile, parsed[1]);
					break;
				case 3: 
					data[i] = new DataFile(coordFile, parsed[1], Integer.parseInt(parsed[2]));
					break;
				case 4: 
					data[i] = new DataFile(coordFile, parsed[1], Integer.parseInt(parsed[2]), createUrl(parsed[3]));
				default: 
					break;
				}
			} catch (Exception e) {				
				e.printStackTrace(System.err);
				return;
			}

		}
	}
}
