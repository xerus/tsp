package tsp;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.geom.*;

import  java.util.Arrays;


public class DrawArea extends JPanel {
	private String[] names; // jména měst
	private Route route;
	private JLabel routeLength; // délka okružní cesty
	private JLabel title;

	public DrawArea() {
		names = null;
		route = new Route();
		title = new JLabel();
		routeLength = new JLabel();

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		title.setHorizontalAlignment(JLabel.CENTER);

		add(title);
		add(route);
		add(routeLength);
		add(Box.createVerticalGlue());
		Dimension d = new Dimension(route.getSize());
		d.setSize(d.getWidth(), d.getHeight() + 30);
		setPreferredSize(d);
	}

	/**
	 * Nastaví nadpis nad vykreslovaný chromozom
	 * @param title Nadpis
	 */
	public void setTitle(String title) {
		this.title.setText(title);
	}

	public void clear() {
		this.setTitle("");
		setCities(null);
		this.routeLength.setText("");
		this.route.setRoute(null);
	}

	/**
	 * Nastaví cestu pro vykreslení, vypíše délku cesty a jednotlívé uzly
	 * @param route cesta
	 * @param length délka cesty
	 * @param generation Číslo generace, ve které se vyskytla tato délka
	 */
	public void setRoute(int[] route, int length, int generation) {
		String s = new String();
		int[] normalized = normalizeRoute(route);
		this.route.setRoute(normalized);

		this.routeLength.setText("<html>Délka cesty: <b>"+length+"</b><br>Chromozom generace <b>"+generation+"</b></html>");
	}

	public String getNormalizedRouteText() {
		int[] normalized = getRouteElement().route;
		String s = new String("");

		if (normalized != null) {
			for (int index = 0; index < normalized.length; index++) {
				s += getCityName(normalized[index]) + " - ";			
			}
			s += getCityName(normalized[0]); // návrat do prvního města
		}
		return s;
	}

	/**
	 * Zařídí, aby se cesta vypisovala od začátku
	 * @param route cesta
	 */
	private int[] normalizeRoute(int[] route) {
		int[] normalized = new int[route.length];
		int index = 0;
		for (int i = 0; i < route.length; i++) {
			if (route[i] == 0) { // nalezení počátku
				index = i;
				break;
			}
		}
		System.arraycopy(route, index, normalized, 0, route.length - index);
		System.arraycopy(route, 0, normalized, route.length - index, index);

		return normalized;
	}

	private String getCityName(int index) {
		String s = new String();
		s += ((names == null || index >= names.length || names[index].isEmpty()) ? index + 1 : names[index]); // +1 aby to odpovídalo formátu tsp
		return s;
	}


	/**
	 * Nastavení názvů měst
	 * Podle nich se vypisují v seznamu
	 * @param names Názvy měst 
	 */
	public void setCities(String[] names) {
		this.names = names;
	}

	/**
	 * Slouží pro získání objektu, ve kterém probíhá vykreslování cesty
	 * @return Objekt pro kreslení
	 */
	public Route getRouteElement() {
		return route;
	}
}