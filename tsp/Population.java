package tsp;

import java.util.*;

public class Population {
	private EvaluatedChromosome[] population;
	private int[][] distanceMatrix; // matice vzdáleností
	private int size;
	private int maxlen; // pomocná proměnná používáná pro výpočet fitness

	private class EvaluatedChromosome implements Comparable<EvaluatedChromosome> {
		int evaluation;
		Chromosome chromosome;

		EvaluatedChromosome(Chromosome ch) {
			this.chromosome = ch;
			this.evaluation = 0;
		}

		EvaluatedChromosome(EvaluatedChromosome ch) {
			this(new Chromosome(ch.getChromosome()));
			setEvaluation(ch.getEvaluation());
		}

		public int getEvaluation() {
			return this.evaluation;
		}

		public void setEvaluation(int e) {
			this.evaluation = e;
		}

		public Chromosome getChromosome() {
			return this.chromosome;
		}

		public int compareTo(EvaluatedChromosome ch) {
			return this.getEvaluation() - ch.getEvaluation();
		} 
	}

	/**
	 * Konstruktor pro vytvoření počáteční populace
	 * @param size Velikost populace
	 * @param distanceMatrix Čtvercová matice vzdáleností mezi body, počet řádků udává velikost chromozomu
	 */
	public Population(int size, int[][] distanceMatrix) {
		this.size = size;
		this.distanceMatrix = distanceMatrix;
		this.maxlen = countMaxlen();
		int chromosomeSize = distanceMatrix.length;
		population = new EvaluatedChromosome[size];
		for (int i = 0; i < size; i++) {
			population[i] = new EvaluatedChromosome(new Chromosome(chromosomeSize));
		}

		// ohodnocení počáteční populace
		this.evaluate();
	}

	/**
	 * Vytvoří populaci z chromozomů
	 * @param chromosomes Chromozomy
	 * @param distanceMatrix Matice vzdáleností
	 */
	public Population(Chromosome[] chromosomes, int[][] distanceMatrix) {
		this.size = chromosomes.length;
		this.distanceMatrix = distanceMatrix;
		this.maxlen = countMaxlen();

		population = new EvaluatedChromosome[size];
		for (int i = 0; i < size; i++) {
			population[i] = new EvaluatedChromosome(new Chromosome(chromosomes[i]));
		}
		// ohodnocení počáteční populace
		this.evaluate();
	}	

	/**
	 * Vytvoří populaci z chromozomů
	 * @param chromosomes Chromozomy
	 * @param distanceMatrix Matice vzdáleností
	 */
	public Population(EvaluatedChromosome[] chromosomes, int[][] distanceMatrix) {
		this(chromosomes.length, distanceMatrix);

		this.population = new EvaluatedChromosome[size];
		for (int i = 0; i < size; i++) {
			this.population[i] = new EvaluatedChromosome(chromosomes[i]);
		}

		// ohodnocení počáteční populace
		this.evaluate();

	}	

	public Population(Population p) {
		this(p.getEvaluatedChromosomes(), p.getDistanceMatrix());
	}

	public EvaluatedChromosome[] getEvaluatedChromosomes() {
		return this.population;
	}

	public int getSize() {
		return this.size;
	}

	public int[][] getDistanceMatrix() {
		return this.distanceMatrix;
	}
	/**
	 * Ohodnotí jedince v populaci
	 */
	private void evaluate() {
		for (int i = 0; i < getSize(); i++) {
			// ohodnocení je inverze délky cesty
			population[i].setEvaluation(convertLengthFitness(getChromosomeRouteLength(i)));
		}
		// seřazení dle ohodnocení
		Arrays.sort(population);
	}

	public int getChromosomeRouteLength(int index) {
		// chromozom byl ohodnocen, vrací se inverze ohodnocení
		if (population[index].getEvaluation() != 0) return convertLengthFitness(population[index].getEvaluation());

		int sum = 0;
		int[] values = getChromosome(index).getConfiguration(); // konfigurace chromozomu = cestpopulation[index].getEvaluation()a

		for (int i = 0; i < values.length - 1; i++) { // suma vzdáleností mezi městy na cestě
			sum += distanceMatrix[values[i]][values[i+1]];
		}
		sum += distanceMatrix[values[values.length - 1]][values[0]]; // z posledního bodu na první
		
		return sum;
	}

	public int getChromosomeEvaluation(int index) {
		if (population[index].getEvaluation() != 0) return population[index].getEvaluation();
		
		population[index].setEvaluation(convertLengthFitness(getChromosomeRouteLength(index)));

		return population[index].getEvaluation();
	}

	private int convertLengthFitness(int num) {
		return (this.maxlen - num);
	}

	public EvaluatedChromosome getEvaluatedChromosome(int index) {
		return population[index];
	}
	public Chromosome getChromosome(int index) {
		return population[index].getChromosome();
	}

	private int countMaxlen() {
		int sum = 0;
		for (int[] r: this.distanceMatrix) {
			int rmax = 0;
			for (int c : r) {
				if(c > rmax) rmax = c;
			}
			sum += rmax;
		}
		return sum;
	}

	/**
	 * @return Vrací se index chromozomu, jehož ohodnocení bylo nejlepší
	 */
	public int getBestChromosomeIndex() {
		int max = 0; // index s prvku s nejlepším ohodnocením
		for (int i = 0; i < this.getSize(); i++) {
			if (getChromosomeEvaluation(i) > getChromosomeEvaluation(max)) {
				max = i;
			}
		}
		return max;
	}

	/**
	 * Náhodný výběr rodiče 
	 **!!!! dodělat ruletu
	 * @param type Typ výběru rodičů - ruletové kolo, stochastický
	 * @return Vrací dvojici rodičů
	 */
	private int[] parentId(ParentChooseType type) {
		long r1, r2; // náhodně vylosovaná čísla
		int[] ids = new int[2];
		long tmpsum = 0;

		long sum = 0; // výpočet sumy ohodnocení
		for (int s = 0; s < this.getSize(); s++) {
			sum += getChromosomeEvaluation(s);
			
		}

		Random rand = new Random();
		switch (type) {
			case SUS:
				r1 = rand.nextLong() % (sum / 2);
				r2 = sum/2 + r1;			
				break;
			default:				
			case RULETE:
				r1 = rand.nextLong() % sum;
				r2 = rand.nextLong() % sum;
				break;
		}
		ids[0] = ids[1] = -1;
		for (int i = 0; i < this.getSize(); i++) {
			tmpsum +=  getChromosomeEvaluation(i);
			if ((ids[0] < 0) && (r1 < tmpsum)) ids[0] = i;
			if ((ids[1] < 0) && (r2 < tmpsum)) ids[1] = i;
			if ((ids[0] >= 0) && (ids[1] >= 0)) break;
		}
		return ids;
	}

	/**
	 * Použije se pro vytvoření nové populace z populace rodičů a jejich potomků.
	 * Jsou různé metody výběru jedinců do populace:
	 *		Rodiče jsou nahrazeni dětmi
	 *      Zůstanou nejlepší
	 */
	public Population mixPopulations(Population children, ReinsertionType method) {
		EvaluatedChromosome[] chromosomes = new EvaluatedChromosome[getSize()]; 
		int i;
		switch (method) {
			case FITNESS_BASED: // vybírá lepší z rodičů a dětí
				int j, k;
				i = j = k = getSize() - 1;	
				for (; i >= 0 ; i--) { // vkládá nejlepší chromozomy
					EvaluatedChromosome ev;
					if (children.getChromosomeEvaluation(j) > this.getChromosomeEvaluation(k)) {
						ev = children.getEvaluatedChromosome(j);
						j--; // posun indexu v potomku
					} else {
						ev = this.getEvaluatedChromosome(k);
						k--; // posun indexu v rodiči
					}
					chromosomes[i] = ev;
				}
				break;
			case PURE: // rodiče jsou nahrazeni dětmi
				chromosomes = children.getEvaluatedChromosomes();
				break;
			case QUARTER: // rodiče jsou nahrazeni dětmi
				chromosomes = children.getEvaluatedChromosomes();
				int qrt = chromosomes.length / 4; // jedna čtvrtina
				for (i = 0; i < qrt; i++)
					chromosomes[i] = this.getEvaluatedChromosome(chromosomes.length - 1 - i);
				break;
			case UNIFORM: // v populaci má stejnou šanci zůstat rodič i dítě
			default:
				Random rand = new Random();
				for (i = 0; i < getSize(); i++) {
					chromosomes[i] = (rand.nextFloat() < 0.5) ? 
										this.getEvaluatedChromosome(i) : 
										children.getEvaluatedChromosome(i);
				}
				break;
		}

		return new Population(chromosomes, distanceMatrix); 
	}

	/**
	 * Vytvoří novou generaci z náhodně spárovaných rodičů
	 * @param ctype Typ křížení
	 * @param crossoverCoeficient Pravděpodobnost křížení
	 * @param mtype Typ mutace
	 * @param mutationCoeficient Pravděpodobnost mutace
	 * @param ptype Způsob výběru jedinců do nové generace
	 * @return Vrací populaci potomků
	 */
	public Population nextGeneration(CrossoverType ctype, float crossoverCoeficient, MutationType mtype, float mutationCoeficient, ParentChooseType ptype) {
		Chromosome[] children = new Chromosome[getSize()];
		Random rand = new Random();

		// rekombinace
		int i = 0;
		while (i < children.length) {
			Chromosome f, m;
			Chromosome[] kids;
			
			// nahodny vyber rodicu
			int[] parentids = parentId(ptype);
			f = getChromosome(parentids[0]);
			m = getChromosome(parentids[1]);
			
			// získání dětí rodičů
			kids = (rand.nextFloat() < crossoverCoeficient) ? crossover(f, m, ctype) : crossover(f, m, CrossoverType.NO_CROSSOVER);

			// mutace potomků
			for (Chromosome ch : kids) {
				if (rand.nextFloat() < mutationCoeficient) ch.mutate(mtype);

				// přidání dětí k ostatním
				if (i < children.length)
					children[i++] = ch;			
			}
		}

		// vytvoření populace potomků
		return new Population(children, distanceMatrix);
	}

	/**
	 * Operátor křížení
	 * @param p1 První rodič
	 * @param p2 Druhý rodič
	 * @return Vrací dvojici potomků
	 */
	public Chromosome[] crossover(Chromosome p1, Chromosome p2, CrossoverType type) {
		Chromosome[] children;

		Random rand = new Random();
		int start, end;

		switch (type) {
			case ONE_POINT: // od náhodně vygenerovaného místa se prohodí všechny body
				start = rand.nextInt(p1.length()+1);
				children = crossoverMulti(p1, p2, start, p1.length(), 1);
				break;
			case TWO_POINT: // mezi dvěma náhodně vygenerovanými místy se prohodí body
				start = rand.nextInt(p1.length()+1);
				end = rand.nextInt(p1.length()+1);
				children = crossoverMulti(p1, p2, start, end, 1);
				break;
			case MULTI_POINT: // každý bod konfigurace chromozomu se přehodí s 50% pravděpodobností
				children = crossoverMulti(p1, p2, 0, p1.length(), (float) 0.5);
				break;
			case OX:
				children = crossoverOX(p1, p2);
				break;
			case PMX:
				children = crossoverPMX(p1, p2);
				break;
			case ERX:
				children = crossoverERX(p1, p2);
				break;
			case SCX:
				children = crossoverSCX(p1, p2);
				break;
			case MSCX:
				children = crossoverMSCX(p1, p2);
				break;
			default:
			case NO_CROSSOVER: // pouze zkopíruje rodiče
				children = new Chromosome[] {new Chromosome(p1), new Chromosome(p2)};
				break;
		}		
		return children;
	}


	/**
	 * Vícebodové křížení s využitím indexové tabulky
	 * Používá se i pro jedno a dvoubodové křížení
	 * @param p1 První rodič
	 * @param p2 Druhý rodič
	 * @param start Počáteční bod křížení
	 * @param end Koncový bod křížení
	 * @param probability Pravděpodobnost, že dojde ke křížení
	 * @return Vrací dva potomky
	 */
	private Chromosome[] crossoverMulti(Chromosome p1, Chromosome p2, int start, int end, float probability) {
		int tmp; 		// pomocný bod pro swap
		int[] thisItab;	// indexové tabulky
		int[] parentItab;	
		Random rand = new Random();

		assert(start <= end && end <= p1.length() );

		thisItab = p1.getIndexTable();
		parentItab = p2.getIndexTable();

		for(int i = start; i < end; i++) { // prohozeni prvku rodicu s určitou pravděpodobností
			if (rand.nextFloat() < probability) { 
				tmp = thisItab[i];
				thisItab[i] = parentItab[i];
				parentItab[i] = tmp;
			}
		}

		Chromosome[] arr = {new Chromosome(null, thisItab),
							new Chromosome(null, parentItab)};

		return arr;		
	}


	/**
	 * PMX křížení
	 * Varianta PMX z: Munakata, T.: Fundamentals of the New Artificial Intelligence
	 * @param p1 První rodič
	 * @param p2 Druhý rodič
	 * @return Vrací dva potomky
	 */
	private Chromosome[] crossoverPMX(Chromosome p1, Chromosome p2) {
		int tmp;
		int min, max;  // dva body tvorici hranici
		Random rand = new Random();
		int[][] pairs; // dvojice hodnot, které se prohodí
		Chromosome[] arr = {new Chromosome(p1),
							new Chromosome(p2)};

		min = rand.nextInt(p1.length());
		max = rand.nextInt(p1.length());

		if (min > max) { // mensi cislo bude v min
			tmp = max;
			max = min;
			min = tmp;
		}


		if(max - min > 0) {
			pairs = new int[max-min][arr.length];
			for (int i = 0; i < pairs.length; i++) { // vytvoření dvojic
				pairs[i][0] = arr[0].configuration[min + i];
				pairs[i][1] = arr[1].configuration[min + i];
			}
			// prohozeni prvku podle dvojic
			for (int id = 0; id < arr.length; id++) { // pro každé z dětí
				for (int i = 0; i < pairs.length; i++) { // prohazovat prvky ve zvláštním cyklu je nutné
					int[] index = new int[2];
					int k = 0;					
					for (int j = 0; j < p1.length(); j++) { 
						if ((arr[id].configuration[j] == pairs[i][0]) ||
							(arr[id].configuration[j] == pairs[i][1])) {
							index[k] = j; // uložení pozice prvku
							index[1] = j; // pro případ, kdy se neprohazuje 		
							if(k > 0) break; // našli jsme oba indexy

							k++;
						}
					}
					// vlastní prohození prvků
					tmp = arr[id].configuration[index[0]];
					arr[id].configuration[index[0]] = arr[id].configuration[index[1]];
					arr[id].configuration[index[1]] = tmp;

				}
			}
					
		}
		return arr;		
	}

	/**
	 * OX křížení
	 */
	private Chromosome[] crossoverOX(Chromosome p1, Chromosome p2)  {
		int tmp, min, max;
		Random rand = new Random();

		Chromosome[] arr = {new Chromosome(p2), // první potomek je kopie druhého rodiče
							new Chromosome(p1)};

		min = rand.nextInt(p1.length());
		max = rand.nextInt(p1.length());

		if (min > max) { // mensi cislo bude v min
			tmp = max;
			max = min;
			min = tmp;
		}

		if (max - min > 0) {
			List<Integer> vp1 = new ArrayList<Integer>(); // pro konfiguraci prvního rodiče
			List<Integer> vp2 = new ArrayList<Integer>(); // pro druhého rodiče
			int[] confp1 = p1.getConfiguration();
			int[] confp2 = p2.getConfiguration();

			Integer[] numInt = new Integer[p1.length()]; // slouži pro převod z int na Integer, List používá pouze Integer
			for (int i = 0; i < p1.length(); i++) {
				numInt[i] = new Integer(i);
			}

			for (int i = 0; i < p1.length(); i++) { // převod int[] do vectoru
				vp1.add(numInt[confp1[i]]);
				vp2.add(numInt[confp2[i]]);
			}

			int[] confch1 = arr[0].getConfiguration();
			int[] confch2 = arr[1].getConfiguration();
			for (int i = min; i < max; i++) { // odstranění duplicit z vektorů
				vp1.remove(numInt[confch1[i]]);
				vp2.remove(numInt[confch2[i]]);
			}

			// zkopírování do konfigurace
			for (int i = 0; i < p1.length(); i++) {
				if (i >= min && i < max) continue; // v <min, max) se nechává původní hodnota
				// do ostatních se kopíruje hodnota z vektoru
				confch1[i] = vp1.remove(0);
				confch2[i] = vp2.remove(0);
			}
		}

		return arr;
	}

	/**
	 * ERX křížení, využívá informace o sousedech každého města.
	 * Ze dvou rodičů vzniká jeden potomek.
	 * @param p1 První rodič
	 * @param p2 Druhý rodič
	 * @return Vrací pole obsahující jednoho potomka
	 */
	private Chromosome[] crossoverERX(Chromosome p1, Chromosome p2)  {
		Random rand = new Random();
		int conflen = p1.length();
		Chromosome chr = new Chromosome(p1); // kopie chromozomu
		int[] chromosome = chr.getConfiguration();
		List<Set<Integer>> edgemap = new ArrayList<Set<Integer>>();

		int[] confp1 = p1.getConfiguration();
		int[] confp2 = p2.getConfiguration();

		List<Integer> unusedCities = new ArrayList<Integer>(); // seznam nepoužitých měst
		Integer[] numInt = new Integer[conflen]; // slouži pro převod z int na Integer
		for (int i = 0; i < conflen; i++) {
			numInt[i] = new Integer(i);
			unusedCities.add(numInt[i]);
		}

		// vytvoření edgemapy
		for (int i = 0; i < conflen; i++) {
			edgemap.add(new HashSet<Integer>());
		}

		for (int i = 1; i < conflen - 1; i++) {
			edgemap.get(confp1[i]).add(numInt[confp1[i+1]]);
			edgemap.get(confp2[i]).add(numInt[confp2[i+1]]);
			edgemap.get(confp1[i]).add(numInt[confp1[i-1]]); // předchůdce
			edgemap.get(confp2[i]).add(numInt[confp2[i-1]]);
		}
		// pro první a poslední index 
		if (conflen > 0) {
			edgemap.get(confp1[0]).add(numInt[confp1[conflen-1]]); 
			edgemap.get(confp2[0]).add(numInt[confp2[conflen-1]]);
			edgemap.get(confp1[conflen-1]).add(numInt[confp1[0]]); 
			edgemap.get(confp2[conflen-1]).add(numInt[confp2[0]]);
			if (conflen > 1) { 
				edgemap.get(confp1[0]).add(numInt[confp1[1]]); 
				edgemap.get(confp2[0]).add(numInt[confp2[1]]);
				edgemap.get(confp1[conflen-1]).add(numInt[confp1[conflen - 2]]); 
				edgemap.get(confp2[conflen-1]).add(numInt[confp2[conflen - 2]]);
			}
		}

		Set<Integer> entries;
		int index = rand.nextInt(conflen);
		entries = edgemap.get(index);
		for (int i = 0; i < conflen; i++) {
			chromosome[i] = index;
			if ((i + 1) >= conflen) break; // vyřazení posledního prvku

			for (Set<Integer> s : edgemap) s.remove(numInt[index]); // odstranění města ze všech edgemap
			unusedCities.remove(numInt[index]); // a ze seznamu nepoužitých měst

			entries = edgemap.get(index);
			if (entries.size() != 0) {
				int min = 0;
				Integer[] cities = entries.toArray(new Integer[0]);
				for (int j = 0; j < entries.size(); j++) {
					if (edgemap.get(cities[j]).size() < edgemap.get(cities[min]).size()) { 
						min = j; // výběr města s nejméně sousedy						
					}
				}
				index = cities[min];
			} else { // náhodný výběr nepoužitého města
				index = unusedCities.get(rand.nextInt(unusedCities.size()));
			}			
		}

		return new Chromosome[]{chr};
	}

	/**
	 * SCX křížení, využívá informace o vzdálenostech mezi městy.
	 * Ze dvou rodičů vzniká jeden potomek.
	 * @param První rodič
	 * @param Druhý rodič
	 * @return Vrací pole obsahující jednoho potomka
	 */
	private Chromosome[] crossoverSCX(Chromosome p1, Chromosome p2)  {
		Random rand = new Random();
		int conflen = p1.length();
		int index;
		Chromosome chr = new Chromosome(p1); // kopie chromozomu
		int[] chromosome = chr.getConfiguration();
		Integer[][] neighbourmap = new Integer[conflen][2];

		int[] confp1 = p1.getConfiguration();
		int[] confp2 = p2.getConfiguration();

		Set<Integer> unusedCities = new HashSet<Integer>(); // seznam nepoužitých měst
		Integer[] numInt = new Integer[conflen]; // slouži pro převod z int na Integer
		for (int i = 0; i < conflen; i++) {
			numInt[i] = new Integer(i);
			unusedCities.add(numInt[i]);
		}

		// vytvoření seznamu následníků pro každé město z obou rodičů
		for (int i = 0; i < conflen - 1; i++) {
			neighbourmap[confp1[i]][0] = numInt[confp1[i+1]];
			neighbourmap[confp2[i]][1] = numInt[confp2[i+1]];
		}

		// vytvoření nového chromozomu
		index = confp1[0];
		for (int i = 0; i < conflen; i++) {
			chromosome[i] = index;
			if ((i + 1) >= conflen) break; // byl to poslední prvek

			unusedCities.remove(numInt[index]); // odstranění ze seznamu
			for (Integer[] s : neighbourmap) {
				if (s[0] == numInt[index]) s[0] = null;
				if (s[1] == numInt[index]) s[1] = null;
			}

			// nalezení následníka
			int min = 0;
			Integer[] cities = neighbourmap[index]; // následníci města "index"
			if (cities[0] == null || cities[1] == null) { // seznam následníků města je prázdný, bude se vybírat v seznamu nepoužitých měst
				cities = unusedCities.toArray(new Integer[0]);
			} 
			for (int j = 1; j < cities.length; j++) { // vybrání bližšího následníka
				if (distanceMatrix[index][cities[j]] < distanceMatrix[index][cities[min]]) min = j;
			}
			index = cities[min]; // nejbližší soused je v cities[min]
		}

		return new Chromosome[]{chr};
	}

	/**
	 * MSCX křížení, využívá informace o vzdálenostech mezi městy.
	 * Jedná se o verzi SCX, která vznikla omylem. Rozdíl je v hranové tabulce.
	 * Ze dvou rodičů vzniká jeden potomek.
	 * @param První rodič
	 * @param Druhý rodič
	 * @return Vrací pole obsahující jednoho potomka
	 */
	private Chromosome[] crossoverMSCX(Chromosome p1, Chromosome p2)  {
		Random rand = new Random();
		int conflen = p1.length();
		int index;
		Chromosome chr = new Chromosome(p1); // kopie chromozomu
		int[] chromosome = chr.getConfiguration();
		List<Set<Integer>> neighbourmap = new ArrayList<Set<Integer>>();

		int[] confp1 = p1.getConfiguration();
		int[] confp2 = p2.getConfiguration();

		Set<Integer> unusedCities = new HashSet<Integer>(); // seznam nepoužitých měst
		Integer[] numInt = new Integer[conflen]; // slouži pro převod z int na Integer
		for (int i = 0; i < conflen; i++) {
			numInt[i] = new Integer(i);
			unusedCities.add(numInt[i]);
		}

		// vytvoření seznamu následníků pro každé město z obou rodičů
		for (int i = 0; i < conflen; i++) {
			neighbourmap.add(new HashSet<Integer>());
		}
		for (int i = 0; i < conflen - 1; i++) {
			neighbourmap.get(confp1[i]).add(numInt[confp1[i+1]]);
			neighbourmap.get(confp2[i]).add(numInt[confp2[i+1]]);
		}

		// vytvoření nového chromozomu
		index = confp1[0];
		for (int i = 0; i < conflen; i++) {
			chromosome[i] = index;
			if ((i + 1) >= conflen) break; // byl to poslední prvek

			unusedCities.remove(numInt[index]); // odstranění ze seznamů
			for (Set<Integer> s : neighbourmap) s.remove(numInt[index]);

			// nalezení následníka
			int min = 0;
			Integer[] cities = neighbourmap.get(index).toArray(new Integer[0]); // následníci města "index"
			if (cities.length == 0) { // seznam následníků města je prázdný, bude se vybírat v seznamu nepoužitých měst
				cities = unusedCities.toArray(new Integer[0]);
			} 
			for (int j = 1; j < cities.length; j++) { // vybrání bližšího následníka
				if (distanceMatrix[index][cities[j]] < distanceMatrix[index][cities[min]]) min = j;
			}
			index = cities[min]; // nejbližší soused je v cities[min]
		}

		return new Chromosome[]{chr};
	}



}