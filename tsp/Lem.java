package tsp;

import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;

class Lem {
	private ArrayList<Chromosome> population;
	private int populationSize;
	private AQ aqlearner;
	private int hGroupSize, lGroupSize;
	private int noChangeLimit;
	private double noChangeFitness;
	private int eliteSize;
	private double [][] specimen;
	private Random rnd;

	public Lem(int populationSize, double hpThreshold, double lpThreshold, double [][] specimen) {
		this.populationSize = populationSize;
		this.aqlearner = new AQ();
		this.hGroupSize = (int)Math.round(hpThreshold * populationSize);
		this.lGroupSize = (int)Math.round(lpThreshold * populationSize);
		this.specimen = specimen;
		this.rnd = new Random();
		this.eliteSize = (int)Math.round(0.1 * populationSize);
		this.noChangeLimit = 20;
		this.noChangeFitness = -99999999;
		this.startOver();
	}

	/**
	 * Jeden krok - generace
	 * Probehne uceni algoritmem AQ a vytvoreni pravidel
	 * Z pravidel se vygeneruji jedinci
	 */
	public void step() {
		if (noChangeLimit > 0) {
			ArrayList<Chromosome> newPopulation = new ArrayList<Chromosome>();
			Chromosome [] negatives = new Chromosome[lGroupSize];
			Chromosome [] positives = new Chromosome[hGroupSize];
			ArrayList<Rule> hypothesis;
			//Collections.sort(population);
			double fit = bestChromosomes().get(0).getFitness();
			if (fit > noChangeFitness) {
				noChangeFitness = fit;
			} else {
				noChangeLimit--;
			}
			for (int i = 0; i < lGroupSize; i++) {
				negatives[i] = population.get(i);
			}
			for (int i = 0; i < hGroupSize; i++) {
				positives[i] = population.get(population.size() - 1 - i);
			}
			hypothesis = aqlearner.allrules(positives, negatives);
			int elites = 0;
			int newpopsize = 0;
			while (elites < eliteSize && newpopsize < populationSize) { // zachovani elity
				newPopulation.add(population.get(population.size()-1-elites));
				newpopsize++;
				elites++;
			}
			if (hypothesis != null)
			while (newpopsize < populationSize) {
				newPopulation.add(randomChromosomeByRule(hypothesis.get(newpopsize % (hypothesis.size()))));
				newpopsize++;
			}
			this.population = newPopulation;
		} else {
			this.noChangeLimit = 10;
			this.noChangeFitness = -99999999;
			startOver();
		}
	}

	private void startOver() {
		this.population = new ArrayList<Chromosome>();
		for (int i = 0; i < populationSize; i++) {
			population.add(randomChromosome());
		}		
	}

	public ArrayList<Chromosome> getPopulation() {
		return population;
	}

	public ArrayList<Chromosome> bestChromosomes() {
		ArrayList<Chromosome> bests = new ArrayList<Chromosome>();
		Collections.sort(population);
		double bestfitness = population.get(population.size() - 1).getFitness();
		for (int i = population.size() - 1; i >= 0; i--) {
			Chromosome ch = population.get(i);
			if (ch.getFitness() >= bestfitness) {
				bests.add(ch);
			} else {
				break;
			}
		}
		return bests;
	}

	private Chromosome randomChromosomeOfPopulation() {
		return population.get(rnd.nextInt(population.size()));
	}

	private Chromosome randomChromosome() {
		int [] representation = new int[specimen.length];
		for (int i = 0; i < specimen.length; i++) {
			representation[i] = randomIncInc((int)Math.round(specimen[i][0]), 
											 (int)Math.round(specimen[i][1]));
		}
		return new Chromosome(representation);
	}

	private Chromosome randomChromosomeByRule(Rule r) {
		Condition [] conditions = r.getConditions();
		int [] representation = new int[specimen.length];
		for (int i = 0; i < conditions.length; i++) {
			int min, max;
			switch (conditions[i].getType()) {
			case LESS:
				min = (int) Math.round(specimen[i][0]);
				max = (int) Math.round(conditions[i].getValue());
				representation[i] = randomIncExc(min, max);
				break;
			case MORE:
				min = (int) Math.round(conditions[i].getValue());
				max = (int) Math.round(specimen[i][1]);
				representation[i] = randomExcInc(min, max);
				break;
			case RANGE:
				min = (int) Math.round(conditions[i].getLowBound());
				max = (int) Math.round(conditions[i].getHighBound());
				representation[i] = randomExcExc(min, max);
				break;
			case TRUE:
			default: // hodnota libovolneho jedince v populaci
				representation[i] = randomChromosomeOfPopulation().getRepresentation()[i];
				break;
			}
		}
		return new Chromosome(representation);
	}

	private int randomExcExc(int min, int max) {
		return rnd.nextInt(max - min - 1) + 1 + min;
	}

	private int randomIncExc(int min, int max) {
		return rnd.nextInt(max - min) + min;
	}

	private int randomExcInc(int min, int max) {
		return rnd.nextInt(max - min) + 1 + min;
	}

	private int randomIncInc(int min, int max) {
		return rnd.nextInt(max - min + 1) + min;
	}

	public String toString() {
		String s = "Population: [";
		for (Chromosome c: population) {
			s += c.toString() +", ";
		}
		char [] tmp_string = s.toCharArray();
		tmp_string[s.length() - 2] = ']';
		return String.valueOf(tmp_string);
	}
} 