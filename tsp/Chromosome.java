package tsp;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.Random;

/**
 * Základní třída představující chromozom
 */
public class Chromosome {
	public int[] configuration; // konfigurace chromozomu
	private int[] indexTable;

	/**
	 * Konstruktor vytváří chromozom o zvolené velikosti,
	 * náhodně vygeneruje počáteční konfiguraci
	 * @param size Velikost chromozomu
	 */
	public Chromosome(int size) {
		String values[] = new String[size]; // pomocne pole pro hodnoty v <0, size)

		this.configuration = new int[size];
		indexTable = null;

		for (Integer i = 0; i < size; i++) { // inicializace konfiguraces
			values[i] = Integer.toString(i);
		}
		List<String> list = Arrays.asList(values);
		Collections.shuffle(list);
		for (int i = 0; i < size; i++) {
			this.configuration[i] = Integer.parseInt(list.get(i));
		}
	}

	/**
	 * Vytváří chromozom ze zadané konfigurace 
	 * @param configuration konfigurace chromozomu
	 */
	public Chromosome(int[] configuration) {
		if (configuration != null) {
			this.configuration = Arrays.copyOf(configuration, configuration.length);
		}
		else this.configuration = configuration;
	}
	
	/**
	 * Vytváří chromozom ze zadané konfigurace a indexové tabulky
	 * @param configuration konfigurace chromozomu
	 * @param indexTable indexová tabulka
	 */
	public Chromosome(int[] configuration, int[] indexTable) {
		this(configuration);
		this.setIndexTable(indexTable);
	}

	/**
	 * Kopírovací konstruktor
	 */
	public Chromosome(Chromosome ch) {
		this(ch.configuration);
	}


	public int[] getConfiguration() {
		if (configuration == null) {
			this.fromIndexTable();
		}
		return configuration;
	}

	/**
	 * Zjistí velikost chromozomu
	 * @return Vrací délku konfigurace
	 */
	public int length() {
		return (configuration != null) ? getConfiguration().length : getIndexTable().length;
	}


	/**
	 * Indexová reprezentace konfigurace chromozomu
	 * @return Vrací konfiguraci v indexovém formátu
	 */
	public int[] getIndexTable() {
		if(indexTable == null) {
			this.toIndexTable();			
		}
		return indexTable;
	}

	/**
	 * Nastaví indexovou tabulku
	 * @param table Nová tabulka
	 */
	public void setIndexTable(int[] table) {
		indexTable = table;
		this.fromIndexTable();
	}

	/**
	 * Převede konfiguraci na indexový formát
	 */	
	private void toIndexTable() {
		Vector<Integer> v = new Vector<Integer>();

		Integer[] numInt = new Integer[this.length()]; // slouži pro převod z int na Integer
		for (int i = 0; i < this.length(); i++) {
			numInt[i] = new Integer(i);
			v.add(numInt[i]);
		}

		if (indexTable == null) {
			indexTable = new int[this.length()];
		}

		int[] conf = this.getConfiguration();
		for (int i = 0; i < this.length() && !v.isEmpty(); i++) {
			indexTable[i] = v.indexOf(numInt[conf[i]]);
			v.remove(indexTable[i]);
		}
	}

	/**
	 * Převede indexový formát na konfiguraci
	 */	
	private void fromIndexTable() {
		int i;
		Vector<Integer> v = new Vector<Integer>();

		for (i = 0; i < this.length(); i++) {
			v.add(i);
		}

		if (configuration == null) {
			configuration = new int[this.length()];
		}

		if(indexTable != null) {
			for (i = 0; i < this.length() && !v.isEmpty(); i++) {
				configuration[i] = v.get(indexTable[i]);
				v.remove(indexTable[i]);
			}
		}		
	}

	/**
	 * Operátor mutace
	 * Aplikuje mutaci dle zvolenáho typu
	 * @param type Druh mutace
	 */
	public void mutate(MutationType type) {
		int min, max, i;
		Random rand = new Random();

		min = rand.nextInt(this.length());
		i = rand.nextInt(this.length());

		switch (type) {
			case EXCHANGE: // výměna dvou měst - vytvoří 4 nové hrany
				mutateExchange(min, i);
				break;
			case INVERSION: // otočení úseku - vytvoří 2 nové hrany
				max = (min < i) ? i : min; // v min bude menší, v max větší
				min = (min < i) ? min : i;
				while (min < max) {
					mutateExchange(min, max);
					min++;
					max--; 
				}
				break;
			case SHUFFLE: // zamíchání konfigurace, což je vytvoření nové konfigurace
				Chromosome ch = new Chromosome(this.length());				
				this.configuration = Arrays.copyOf(ch.getConfiguration(), this.length());
				break;
		}

		if (indexTable != null) { // pokud byla použita indexová tabulka, je nutné ji smazat,
			indexTable = null;  // protože došlo ke změně konfigurace
		}
	}


	/**
	 * Provede výměnu dvou měst na zadaných pozicích
	 * používá se u mutace výměny, inverze.
	 * @param a První pozice
	 * @param b Druhá pozice
	 */
	private void mutateExchange(int a, int b) {
		int tmp, min, max;
		min = (a < b) ? a : b;
		max = (a < b) ? b : a;

		int[] conf = getConfiguration();

		tmp = conf[min]; // prohození
		conf[min] = conf[max];
		conf[max] = tmp;
	}

	/**
	 * Pro výpis chromozomu,
	 * vypisuje konfigurace chromozomu jako pořadí indexů měst
	 */
	public String toString() {
		String s = "";
		int[] conf = getConfiguration();
		for (int i = 0; i < this.length(); i++) {
			s += conf[i] + " ";
		}
		return s;
	}
}