package tsp;

import java.util.Vector;
import java.lang.String;

class Rule {
	private Chromosome base;
	private Condition [] conditions;

	public Rule(Chromosome base) {
		this.base = base;
		this.conditions = new Condition[base.length()];
		for (int i = 0; i < base.length(); i++) {
			conditions[i] = new Condition();
		}
	}

	public Rule(Chromosome base, Condition [] conditions) {
		this.base = base;
		this.conditions = conditions;
	}

	public boolean covers(Chromosome ch) {
		for (int i = 0; i < base.length(); i++) {
			if (! conditions[i].cover(ch.getRepresentation()[i])) {
				return false;
			}
		}
		return true;
	}

	public Condition [] getConditions() {
		return conditions;
	}

	/**
	 * Mnozina pravidel pokryvajici base, ale nepokryvajici negative
	 */
	public Vector<Rule> notCovering(Chromosome negative) {
		Vector<Rule> notCoveringRules = new Vector<Rule>();
		if (!this.covers(negative)) {
			notCoveringRules.add(this);
		} else for (int i = 0; i < conditions.length; i++) {
			if (conditions[i].cover(negative.getRepresentation()[i]) 
				&& base.getRepresentation()[i] != negative.getRepresentation()[i]) {
				Condition [] tmp_conditions = new Condition[conditions.length];
				Condition actual_condition;
				System.arraycopy(conditions, 0, tmp_conditions, 0, conditions.length);
				if (base.getRepresentation()[i] > negative.getRepresentation()[i]) {
					actual_condition = new Condition(ConditionType.MORE, negative.getRepresentation()[i]);
				} else {
					actual_condition = new Condition(ConditionType.LESS, negative.getRepresentation()[i]);
				}
				tmp_conditions[i] = new Condition(actual_condition, conditions[i]);
				notCoveringRules.add(new Rule(base, tmp_conditions));

			}
		}
		return notCoveringRules;
	}

	public String toString() {
		String s = "RULE: [";
		for (Condition c: this.conditions) {
			s += c.toString() +", ";
		}
		char [] tmp_string = s.toCharArray();
		tmp_string[s.length() - 2] = ']';
		return String.valueOf(tmp_string);
	}
}