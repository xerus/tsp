package tsp;

class Specimen {

	private double [][] ranges;
	
	public Specimen(double [] lowBounds, double [] highBounds) {
		ranges = new double [lowBounds.length][2];
		for (int i = 0; i < lowBounds.length; i++) {
			ranges[i][0] = lowBounds[i];
			ranges[i][1] = highBounds[i];
		}
	}

	public double [][] getRanges() {
		return ranges;
	}
}