package tsp;

import java.util.*;
import java.util.Vector;

class Fitness {
	
	private int[][] distanceMatrix;
 
	public Fitness(int[][] distanceMatrix) {
		this.distanceMatrix = distanceMatrix;
	}

	public double eval(int [] values) {
		int sum = 0;
		//int[] values = convertFromIndex(individual.getRepresentation()); // konfigurace chromozomu = cestpopulation[index].getEvaluation()a

		for (int i = 0; i < values.length - 1; i++) { // suma vzdáleností mezi městy na cestě
			sum += distanceMatrix[values[i]][values[i+1]];
		}
		sum += distanceMatrix[values[values.length - 1]][values[0]]; // z posledního bodu na první

		return 0 - sum;
	}
}