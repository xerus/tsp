package tsp;

import java.util.Arrays;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Vector;

class AQ {
	private int maxstar;

	public AQ(int maxstar) {
		this.maxstar = maxstar;
	}

	public AQ() {
		this(10);
	}

	public ArrayList<Rule> allrules(Chromosome[] positives, Chromosome[] negatives) {
		ArrayList<Rule> rules = new ArrayList<Rule>();
		ArrayList<Chromosome> uncovered = new ArrayList<Chromosome>(Arrays.asList(positives));

		while (!uncovered.isEmpty()) {
			Chromosome seed = uncovered.remove(0);
			Rule coverSeed = this.aqrule(seed, negatives);
			rules.add(coverSeed);
			ArrayList<Chromosome> tmp_uncovered = new ArrayList<Chromosome>(uncovered);
			for (Chromosome example : uncovered) {
				if (coverSeed.covers(example)) {
					tmp_uncovered.remove(example);
				}
			}
			uncovered = tmp_uncovered;
		}

		return rules;
	}
	
	public Rule aqrule(Chromosome seed, Chromosome[] negatives) {
		ArrayList<Rule> star = new ArrayList<Rule>();
		Rule start = new Rule(seed);
		star.add(start);
		for (Chromosome neg : negatives) {
			ArrayList<Rule> tmp_star = new ArrayList<Rule>(star);
			for (Rule r : star) {
				if (r.covers(neg)) {
					tmp_star.remove(r);
					for (Rule notCovR : r.notCovering(neg)) {
						tmp_star.add(notCovR);
					}	
				}
			}
			Collections.shuffle(tmp_star);
			while (tmp_star.size() > this.maxstar) { // spravne by se mely odstranit "nejhorsi" ze star
				tmp_star.remove(0);
			}
			star = tmp_star;
		}
		Collections.shuffle(star);
		if (star.size() > 0) {
			return star.get(0);			
		} else {
			return start;
		}
	}
}