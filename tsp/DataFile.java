package tsp;

import java.io.*;
import java.net.*;
import java.lang.Math;

/**
 * Pomocná struktura pro práci se soubory obsahující informace o městech
 */
public class DataFile {
	private URL coordFile; // cesta k souboru
	private String title; // název, který se zobrazí v liště
	private int count; // počet měst

	private double[][] coords; // souřadnice měst
	private int[][] distanceMatrix; // vzájemné vzdálenosti měst měst
	private String[] names; // pole názvů měst
	private URL nameFile; // cesta k souboru s názvy měst

	static String charset = "UTF-8";

	public DataFile(URL coordFile) {
		this.coordFile = coordFile;
		this.count = 0;
		this.title = "";
		this.names = null;
	}

	DataFile(URL coordFile, String title) {
		this(coordFile);
		this.title = title;
	}
	DataFile(URL coordFile, String title, int count) {
		this(coordFile, title);
		this.count = count;
	}

	DataFile(URL coordFile, String title, int count, URL nameFile) {
		this(coordFile, title, count);
		this.nameFile = nameFile;
	}

	public double[][] getCoordinates() {
		if (coords == null) parseCoordFile();

		return coords;
	}

	public int[][] getDistanceMatrix() {
		if (distanceMatrix == null) parseCoordFile();

		return distanceMatrix;
	}

	public String[] getCityNames() {
		if ((names == null) && (nameFile != null)) parseNameFile();

		return names;
	}

	/**
	 * Projde soubor se souřadnicemi, 
	 * zpracuje souřadnice a nastaví matici vzdáleností a souřadnic
	 */
	private void parseCoordFile() {
		BufferedReader br;
		String line;
		int state = 0;
		String edgeWeight = new String();
		String edgeFormat = new String();
		String displayData = new String("COORD_DISPLAY");

		
		try {
			br = new BufferedReader(new InputStreamReader(coordFile.openStream(), charset));
			while ((line = br.readLine()) != null) {
				if (line.startsWith("DIMENSION")) count = Integer. parseInt(line.split("\\s*:\\s*")[1]);
				else if (line.startsWith("EDGE_WEIGHT_TYPE")) edgeWeight = line.split("\\s*:\\s*")[1];
				else if (line.startsWith("EDGE_WEIGHT_FORMAT")) edgeFormat = line.split("\\s*:\\s*")[1];
				else if (line.startsWith("NODE_COORD_SECTION") || line.startsWith("DISPLAY_DATA_SECTION")) {
					coords = new double[count][2]; // alokace místa pro souřadnice
					for (int i = 0; i < count; i++) {
						String dataline = br.readLine().trim();
						String[] data = dataline.split("\\s+");
						coords[i][0] = Double.parseDouble(data[1]);
						coords[i][1] = Double.parseDouble(data[2]);
					}
				} else if (line.startsWith("EDGE_WEIGHT_SECTION")) { // načtení matice vzdáleností					
					distanceMatrix = new int[count][count];
					if (edgeFormat.startsWith("FULL_MATRIX"))
					for (int i = 0; i < count; i++) {
						String dataline = br.readLine().trim();
						String[] data = dataline.split("\\s+");
						for (int j = 0; j < count; j++) {
							distanceMatrix[i][j] = Integer.parseInt(data[j]);
						}
					}
				} 
			}
			// spočítání matice vzdáleností
			if (edgeWeight.startsWith("EUC_2D")) {
				if(distanceMatrix == null) distanceMatrix = new int[count][count];
				for (int i = 0; i < count; i++) {
					for (int j = i; j < count; j++) {
						if (j == i) {
							distanceMatrix[i][j] = 0;
							continue;
						}
						double dx = coords[i][0] - coords[j][0];
						double dy = coords[i][1] - coords[j][1];
						distanceMatrix[i][j] = distanceMatrix[j][i] = Math.round((float)Math.sqrt(dx*dx +dy*dy));
					}
				}
			}

		} catch (Exception e) {
			System.err.println("Nezdařilo se čtení souboru "+coordFile);
			System.exit(1);
		}
	}

	private void parseNameFile() {
		BufferedReader br;
		String line;

		try {
			int i = 0;
			br = new BufferedReader(new InputStreamReader(nameFile.openStream(), charset));
			names = new String[count];
			while ((line = br.readLine()) != null) {
				if(i >= count) break;
				names[i++] = line.trim().split("\\d\\s+")[1]; // format radku je: index nazev
			}
		} catch (Exception e) {
			System.err.println("Nezdařilo se čtení souboru se jmény: "+nameFile);
			names = null;
		}

	}

	public String toString() {
		String s = new String("");

		s += (title == null) ? coordFile : title;
		if (count != 0)	s += " ("+count+")";

		return s;
	}
}